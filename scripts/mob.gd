class_name Mob extends CharacterBody2D

const SPEED = 300
const LIMIT = 10

var waypoint_list = []
var target = null
var last1 = null
var last2 = null

func set_waypoints(val):
	waypoint_list = val

# Called when the node enters the scene tree for the first time.
func _ready():
	velocity = Vector2(-100, 0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _physics_process(delta):
	# Find a new waypoint
	if target == null:
		var closest = null
		for t in range(0, len(waypoint_list)):
			if waypoint_list[t] in [last1, last2]:
				continue
			
			if closest == null:
				closest = t
			elif waypoint_list[closest].distance_to(transform.origin) > waypoint_list[t].distance_to(transform.origin):
				closest = t
		target = waypoint_list[closest]
		
	# Move towards waypoint
	var distance = (target - transform.origin)
	if distance.length() > LIMIT:
		velocity = distance.normalized() * SPEED
		move_and_slide()
	else:
		last2 = last1
		last1 = target
		transform.origin = target
		target = null
