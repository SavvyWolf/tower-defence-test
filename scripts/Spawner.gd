extends Node2D

var waypoint_list = []
var mob_scene = preload("res://scenes/mob.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	# Get all waypoints
	for waypoint in %Waypoints.get_children():
		waypoint_list.append(waypoint.transform.get_origin())
	print(waypoint_list)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_spawn_timer_timeout():
	var child = mob_scene.instantiate()
	child.set_waypoints(waypoint_list)
	child.transform = transform
	get_parent().add_child(child);
